package com.example.crud.common;

import lombok.Data;

@Data
public class MessageCodeResult<T> {

    private int code;
    private String message;
    private T result;

    public MessageCodeResult(int code, String message, T result) {
        this.code = code;
        this.message = message;
        this.result = result;
    }

    public static <T> MessageCodeResult<T> ok(T result) {
        return new MessageCodeResult<>(0, "ok", result);
    }

    public static <T> MessageCodeResult<T> fail(String message) {
        return new MessageCodeResult<>(500, message, null);
    }

    public static <T> MessageCodeResult<T> build(int code, String message) {
        return new MessageCodeResult<>(code, message, null);
    }
}
