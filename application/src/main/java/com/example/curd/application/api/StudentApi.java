package com.example.curd.application.api;

import com.example.crud.common.MessageCodeResult;
import com.example.curd.application.command.StudentCommandService;
import com.example.curd.application.dto.StudentAddDTO;
import com.example.curd.application.dto.StudentEditDTO;
import com.example.curd.application.dto.StudentListItemDTO;
import com.example.curd.application.query.StudentQueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentApi {

    private final StudentCommandService commandService;
    private final StudentQueryService queryService;

    @PostMapping
    public MessageCodeResult<Void> add(@RequestBody StudentAddDTO dto) {
        commandService.add(dto);
        return MessageCodeResult.ok(null);
    }

    @PutMapping("/{id}")
    public MessageCodeResult<Void> update(@PathVariable("id") String id, @RequestBody StudentEditDTO dto) {
        commandService.update(id, dto);
        return MessageCodeResult.ok(null);
    }

    @GetMapping
    public List<StudentListItemDTO> list() {
        return queryService.findAll();
    }

}
