package com.example.curd.application.query;

import com.example.curd.application.dto.StudentListItemDTO;

import java.util.List;

public interface StudentQueryService {
    /**
     * 假如是一个报表模型
     * <p>
     * 如果data模块定义了报表DTO也可以直接使用，减少一次 assembler 的转换
     *
     * @return list
     */
    List<StudentListItemDTO> findAll();
}
