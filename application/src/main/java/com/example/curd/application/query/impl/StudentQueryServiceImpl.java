package com.example.curd.application.query.impl;

import com.example.crud.data.repository.StudentRepository;
import com.example.curd.application.assembler.StudentAssembler;
import com.example.curd.application.dto.StudentListItemDTO;
import com.example.curd.application.query.StudentQueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentQueryServiceImpl implements StudentQueryService {

    private final StudentRepository repository;
    private final StudentAssembler assembler = StudentAssembler.INSTANCE;

    @Override
    public List<StudentListItemDTO> findAll() {
        // Query 直接调用 repository
        return repository.findAll().stream().map(assembler::toDTO).collect(Collectors.toList());
    }
}
