package com.example.curd.application.command;

import com.example.curd.application.dto.StudentAddDTO;
import com.example.curd.application.dto.StudentEditDTO;

public interface StudentCommandService {
    void add(StudentAddDTO dto);
    void update(String id, StudentEditDTO dto);
}
