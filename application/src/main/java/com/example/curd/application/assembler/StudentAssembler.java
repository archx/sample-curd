package com.example.curd.application.assembler;

import com.example.crud.data.dto.StudentListItemQueryDTO;
import com.example.crud.data.entity.Student;
import com.example.curd.application.dto.StudentAddDTO;
import com.example.curd.application.dto.StudentEditDTO;
import com.example.curd.application.dto.StudentListItemDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface StudentAssembler {

    StudentAssembler INSTANCE = Mappers.getMapper(StudentAssembler.class);

    @Mapping(target = "id", ignore = true)
    Student from(StudentAddDTO dto);

    @Mapping(target = "id", ignore = true)
    void updateStudent(StudentEditDTO dto, @MappingTarget Student student);

    StudentListItemDTO toDTO(StudentListItemQueryDTO dto);
}
