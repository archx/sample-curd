package com.example.curd.application.command.impl;

import com.example.crud.data.entity.Student;
import com.example.crud.data.repository.StudentRepository;
import com.example.crud.data.service.StudentService;
import com.example.curd.application.assembler.StudentAssembler;
import com.example.curd.application.command.StudentCommandService;
import com.example.curd.application.dto.StudentAddDTO;
import com.example.curd.application.dto.StudentEditDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentCommandServiceImpl implements StudentCommandService {

    private final StudentService service;
    private final StudentRepository repository;
    private final StudentAssembler assembler = StudentAssembler.INSTANCE;

    @Override
    public void add(StudentAddDTO dto) {
        Student student = assembler.from(dto);
        service.save(student);
    }

    @Override
    public void update(String id, StudentEditDTO dto) {
        service.saveIfPresent(id, student -> assembler.updateStudent(dto, student));
    }
}
