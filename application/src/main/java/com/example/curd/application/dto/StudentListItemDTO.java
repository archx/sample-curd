package com.example.curd.application.dto;

import lombok.Data;

@Data
public class StudentListItemDTO {
    private String id;
    private String name;
    private int gender;
}
