package com.example.curd.application.dto;

import lombok.Data;

@Data
public class StudentAddDTO {
    private String name;
    private int gender;
    private String birthday;
}
