package com.example.myapp.config;

import com.example.crud.infrastructure.persistence.config.PersistenceContextConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(PersistenceContextConfig.class)
@ComponentScan({"com.example.curd.application", "com.example.crud.data"})
public class AppConfig {
}
