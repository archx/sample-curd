package com.example.crud.infrastructure.persistence.dao;

import com.example.crud.infrastructure.persistence.dataobj.StudentDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentJpaRepository extends JpaRepository<StudentDO, String> {
}
