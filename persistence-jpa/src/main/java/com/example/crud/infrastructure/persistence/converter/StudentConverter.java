package com.example.crud.infrastructure.persistence.converter;

import com.example.crud.data.entity.Student;
import com.example.crud.infrastructure.persistence.dataobj.StudentDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StudentConverter {

    StudentConverter INSTANCE = Mappers.getMapper(StudentConverter.class);

    Student toStudent(StudentDO bean);

    StudentDO toStudentDO(Student bean);
}
