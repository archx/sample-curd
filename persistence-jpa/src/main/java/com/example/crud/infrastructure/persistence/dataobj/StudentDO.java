package com.example.crud.infrastructure.persistence.dataobj;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "t_student")
@DynamicUpdate
public class StudentDO {
    @Id
    private String id;
    private String name;
    private int gender;
    private String birthday;
}
