package com.example.crud.data.dto;

import lombok.Data;

/**
 * 提供给CQRS模式的Query使用
 */
@Data
public class StudentListItemQueryDTO {
    private String id;
    private String name;
    private int gender;
}
