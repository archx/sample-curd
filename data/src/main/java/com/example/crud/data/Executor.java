package com.example.crud.data;

@FunctionalInterface
public interface Executor {
    void execute();
}
