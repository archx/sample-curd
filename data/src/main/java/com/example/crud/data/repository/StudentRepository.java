package com.example.crud.data.repository;

import com.example.crud.data.dto.StudentListItemQueryDTO;
import com.example.crud.data.entity.Student;

import java.util.List;
import java.util.Optional;

public interface StudentRepository {

    void save(Student student);

    Optional<Student> findById(String id);

    void update(Student student);

    Optional<Student> removeById(String id);

    boolean exists(String id);

    List<StudentListItemQueryDTO> findAll();
}
