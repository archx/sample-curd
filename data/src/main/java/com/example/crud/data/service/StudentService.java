package com.example.crud.data.service;

import com.example.crud.data.Executor;
import com.example.crud.data.entity.Student;

import java.util.Optional;
import java.util.function.Consumer;

public interface StudentService {

    Optional<Student> get(String id);

    Optional<Student> remove(String id);

    String save(Student student);

    void saveIfAbsent(Student student, Executor executor);

    void saveIfPresent(String id, Consumer<Student> consumer);
}
