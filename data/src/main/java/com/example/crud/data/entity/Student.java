package com.example.crud.data.entity;

import lombok.Data;

@Data
public class Student {
    private String id;
    private String name;
    private int gender;
    private String birthday;
}
