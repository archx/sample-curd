package com.example.crud.infrastructure.persistence.mapper;

import com.example.crud.data.dto.StudentListItemQueryDTO;
import com.example.crud.infrastructure.persistence.dataobj.StudentDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface StudentDOMapper {

    int save(StudentDO record);

    int updateById(StudentDO record);

    StudentDO findById(@Param("id") String id);

    int deleteById(@Param("id") String id);

    int countById(@Param("id") String id);

    List<StudentListItemQueryDTO> findAll();
}
