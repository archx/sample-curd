package com.example.crud.infrastructure.persistence.dataobj;

import lombok.Data;

@Data
public class StudentDO {
    private String id;
    private String name;
    private int gender;
    private String birthday;
}
