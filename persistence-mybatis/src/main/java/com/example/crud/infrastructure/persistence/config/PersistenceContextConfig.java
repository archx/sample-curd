package com.example.crud.infrastructure.persistence.config;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.autoconfigure.ConfigurationCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan("com.example.crud.infrastructure.persistence")
@MapperScan("com.example.crud.infrastructure.persistence.mapper")
public class PersistenceContextConfig {

    @Bean
    public ConfigurationCustomizer configurationCustomizer() {
        return (configuration) -> {
            // 开启下划线转驼峰
            configuration.setMapUnderscoreToCamelCase(true);
        };
    }
}
